\version "2.18.2"

\header {
  title = "“Ing”, “Ing”"
  subtitle = "To the tune of “Skip to my Lou”"
  composer = "Lester"
  arranger = "Assistance by Devin Ulibarri"
  copyright = "CC-BY-SA 2021"
}

global = {
  \key c \major
  \time 4/4
}

classicalGuitar = \relative c' {
  \global
  % Music follows here.
  \repeat volta 2 {
    e4 c4 e16 e16~ e16 e16 g4
    d4 b4 d16 d16~ d16 d16 f4 \break
    e4 c4 e16 e16~ e16 e16 g4
    d8 f e d c4 c
  }
}

\score {
  \new Staff \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar"
  } { \clef "treble_8" \classicalGuitar }
  \layout { }
  \midi {
    \tempo 4=100
  }
}
